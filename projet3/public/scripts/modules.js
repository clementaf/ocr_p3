var APPLICATION = APPLICATION || {};
//throw new Error();

(function() {
    "use strict";

    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* Outils --------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */

    /**
     * Objet Outils
     * @return {[type]} [description]
     */
    APPLICATION.MODULES.Outils = function() {
        return this;
    };

    /**
     * fonction recuperer_donnees_get
     * @param  string   url
     * @param  {Function} callback fonction si reussite
     * @return null
     */
    APPLICATION.MODULES.Outils.prototype.recuperer_donnees_get = function(url, callback) {
        var requeteAjax = new XMLHttpRequest();

        requeteAjax.open("GET", url);

        requeteAjax.addEventListener("load", function () {
            if (requeteAjax.status >= 200 && requeteAjax.status < 400) {
                // Appelle la fonction callback en lui passant la réponse de la requeteAjax
                callback(requeteAjax.responseText);
            } else {
                console.error(requeteAjax.status + " " + requeteAjax.statusText + " " + url);

                return false;
            }
        });

        requeteAjax.addEventListener("error", function () {
            console.error("Erreur réseau avec l'URL " + url);

            return false;
        });

        requeteAjax.send(null);
    };

    /**
     * debugguer un objet
     * @param  objet objet objet à debugguer
     * @return null
     */
    APPLICATION.MODULES.Outils.prototype.debug = function(objet) {
        var text = 'Object {\n';

        for (var i in objet) {
            if (i !== 'debug') {
                text += '    [' + i + '] => ' + this[i] + '\n';
            }
        }
        alert(text + '}');
    };

    /**
     * retourner la taille de la fenetre
     * @return int
     */
    APPLICATION.MODULES.Outils.prototype.donner_largeur_fenetre = function() {
        var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        x = w.innerWidth || e.clientWidth || g.clientWidth,
        y = w.innerHeight|| e.clientHeight|| g.clientHeight;

        // console.log('sur document -> innerWidth : ' + w.innerWidth);
        // console.log('sur document -> clientWidth : ' + w.clientWidth);
        // console.log('sur document -> clientWidth : ' + w.clientWidth);
        //
        // console.log('sur document -> innerWidth : ' + d.innerWidth);
        // console.log('sur document -> clientWidth : ' + d.clientWidth);
        // console.log('sur document -> clientWidth : ' + d.clientWidth);
        //
        // console.log('sur documentElement -> innerWidth : ' + e.innerWidth);
        // console.log('sur documentElement -> clientWidth : ' + e.clientWidth);
        // console.log('sur documentElement -> clientWidth : ' + e.clientWidth);
        //
        // console.log('sur body -> innerWidth : ' + g.innerWidth);
        // console.log('sur body -> clientWidth : ' + g.clientWidth);
        // console.log('sur body -> clientWidth : ' + g.clientWidth);

        return w.innerWidth;
    };

    /**
     * detecter le navigateur
     * @return int
     */
    APPLICATION.MODULES.Outils.prototype.detecter_navigateur = function() {
        //Check if browser is IE
        if (navigator.userAgent.search("MSIE") >= 0) {
            return 0;
        }
        //Check if browser is Chrome
        else if (navigator.userAgent.search("Chrome") >= 0) {
            if(navigator.userAgent.toLowerCase().indexOf("op") > -1) {
                return 4; //opera
            }

            return 1;
        }
        //Check if browser is Firefox
        else if (navigator.userAgent.search("Firefox") >= 0) {
            return 2;
        }
        //Check if browser is Safari
        else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
            return 3;
        }
        //Check if browser is Opera
        else if (navigator.userAgent.search("Opera") >= 0) {
            return 4;
        }

        return false;
    };

    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* Carte ---------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */

    /**
     * Objet Carte
     * @param  string idHtmlMap ID HTML de la carte
     * @return objet           objet Carte
     */
    APPLICATION.MODULES.Carte = function(idHtmlMap) {
        this.map = null;

        this.mapDom = null;

        this.markerInformationDom = null;

        this.markerIdAffiche = null;

        this.statutDom = null;

        this.nomDom = null;

        this.adresseDom = null;

        this.emplacementsDisposDom = null;

        this.velosDisposDom = null;

        this.fermerDom = null;

        this.reserversDom = null;

        this.identifiantDom = null;

        this.MarkerSelectionne = null;

        return this.__construct(idHtmlMap);
    };

    /**
     * constructeur de l'objet Carte
     * @param  string idHtmlMap ID HTML de la carte
     * @return objet           objet Carte
     */
    APPLICATION.MODULES.Carte.prototype.__construct = function(idHtmlMap) {
        var _this = this;

        this.markers = [];

        this.contruire_map(idHtmlMap);

        this.identifiantDom = document.getElementsByClassName('js-identifiant')[0];

        this.markerInformationDom = document.getElementsByClassName('js-markerInformation')[0];

        this.statutDom = document.getElementsByClassName('js-statut')[0];

        this.nomDom = document.getElementsByClassName('js-nom')[0];

        this.adresseDom = document.getElementsByClassName('js-adresse')[0];

        this.emplacementsDisposDom = document.getElementsByClassName('js-emplacementsDispos')[0];

        this.velosDisposDom = document.getElementsByClassName('js-velosDispos')[0];

        this.fermerDom = document.getElementsByClassName('js-fermer');

        this.reserversDom = document.getElementsByClassName('js-reserver');

        for (var i = 0; i < this.fermerDom.length; i++) {
            this.fermerDom[i].addEventListener('click', function(e){ return _this.fermer_marker(e, null);});
        }

        window.addEventListener("resize", function() {
            return _this.ajuster_hauteur();
        });



        return this;
    };

    APPLICATION.MODULES.Carte.prototype.ajuster_hauteur = function() {
        var largeur = APPLICATION.application.outils.donner_largeur_fenetre();
        if(this.markerInformationDom.style.display === 'block') {
            if(largeur >= 900) {
                this.mapDom.style.width = '75%';

                this.markerInformationDom.style.width = '25%';

                this.markerInformationDom.style.display = 'block';
            }else if(largeur >= 700) {
                this.mapDom.style.width = '50%';

                this.markerInformationDom.style.width = '50%';

                this.markerInformationDom.style.display = 'block';
            } else {
                this.mapDom.style.width = '0%';

                this.markerInformationDom.style.width = '100%';

                this.markerInformationDom.style.display = 'block';
            }
        }

        return this;
    };

    /**
     * construire la map Google API
     * @param  string idHtmlMap ID HTML de la carte
     * @return objet           objet Carte
     */
    APPLICATION.MODULES.Carte.prototype.contruire_map = function(idHtmlMap) {
        if(!document.getElementById(idHtmlMap)) {
            console.log('l\id HTML ' + idHtmlMap + ' n\'existe pas.');
            return false;
        }

        this.mapDom = document.getElementById(idHtmlMap);

        if(!APPLICATION.DONNEES.mapOptions) {
            console.log('pas d\'option pour la creation de la map');

            return false;
        }

        var options = APPLICATION.DONNEES.mapOptions;

        this.map = new google.maps.Map(this.mapDom, options);

        return this;
    };

    /**
     * generer l'ensemble des markers
     * @param  array stations tableau de l'ensemble des Stations
     * @return objet          objet Carte
     */
    APPLICATION.MODULES.Carte.prototype.generer_markers = function(stations) {
        for (var i = 0 ; i < stations.length ; i++) {
            this.markers.push(this.generer_marker(stations[i]));
        }

        if(this.markers) {
            this.generer_clusters();
        }
        // console.log(stations);
        // var _this = this;
        //
        // var markerTemporaire = new APPLICATION.MODELES.Marker(stations);
        //
        // markerTemporaire.map = _this.map
        //
        // var marker = new google.maps.Marker(markerTemporaire);
        //
        // google.maps.event.addListener(marker, 'click', function(Event) {
        //     _this.ouvrir_station(marker);
        // });
        //
        // this.canevaQuitterDom.addEventListener('click', function(e){ e.preventDefault();; return _this.canevaDom.style.visibility = 'hidden';});

        return this;
    };

    /**
     * generer un marker
     * @param  objet station objet Station
     * @return objet          objet Marker
     */
    APPLICATION.MODULES.Carte.prototype.generer_marker = function(station) {
        var _this = this;

        var markerTemporaire = new APPLICATION.MODELES.Marker(station);

        markerTemporaire.map = _this.map;

        var marker = new google.maps.Marker(markerTemporaire);

        google.maps.event.addListener(marker, 'click', function(Event) {
            _this.ouvrir_marker(marker);
        });

        // this.canevaQuitterDom.addEventListener('click', function(e){ e.preventDefault();; return _this.canevaDom.style.visibility = 'hidden';});

        return marker;
    };

    /**
     * Génère les groupe de Marker : cluster
     * @return objet          objet Marker
     */
    APPLICATION.MODULES.Carte.prototype.generer_clusters = function() {
        if(!APPLICATION.DONNEES.clusterStyles) {
            console.log('pas de donnees sur les clusteurStyle');
        }
        var clusterStyles = APPLICATION.DONNEES.clusterStyles;

        var options = {
            styles: clusterStyles
        };

        var markerCluster = new MarkerClusterer(this.map, this.markers, options);
    };

    /**
     * afficher le panneau qui décrit la station
     * @param  objet marker
     * @return {[type]}        [description]
     */
    APPLICATION.MODULES.Carte.prototype.ouvrir_marker = function(marker) {
        if(this.markerIdAffiche === marker.idStation) {
            this.fermer_marker(null, marker);

            return true;
        }

        this.markerIdAffiche = marker.idStation;

        this.testMarkSelectionne = marker;

        this.parametrer_affichage_station(APPLICATION.application.stations.stations[this.markerIdAffiche]);

        var largeur = APPLICATION.application.outils.donner_largeur_fenetre();

        if(largeur >= 900) {
            this.mapDom.style.width = '75%';

            this.markerInformationDom.style.width = '25%';

            this.markerInformationDom.style.display = 'block';
        }else if(largeur >= 700) {
            this.mapDom.style.width = '50%';

            this.markerInformationDom.style.width = '50%';

            this.markerInformationDom.style.display = 'block';
        } else {
            this.mapDom.style.width = '0%';

            this.markerInformationDom.style.width = '100%';

            this.markerInformationDom.style.display = 'block';
        }

        return this;
    };

    /**
     * fermer le panneau qui décrit la station
     * @param  objet e             objet Event
     * @param  objet marker  [marker=null]
     * @return {[type]}               [description]
     */
    APPLICATION.MODULES.Carte.prototype.fermer_marker = function(e, marker) {
        if(e !== null && e.preventDefault) {
            e.preventDefault();
        }

        if(marker === undefined) {
            marker = null;
       }

        this.testMarkSelectionne = null;

        this.markerIdAffiche = null;

        this.mapDom.style.width = '100%';

        this.markerInformationDom.style.width = '0%';

        this.markerInformationDom.style.display = 'none';
    };

    /**
     * [description]
     * @param  {[type]} station [description]
     * @return {[type]}         [description]
     */
    APPLICATION.MODULES.Carte.prototype.parametrer_affichage_station = function(station) {
        if(station.statut === "ouverte") {
            this.statutDom.classList.remove("--statut-close");
            this.statutDom.classList.add("--statut-open");
            for (var i = 0; i < this.reserversDom.length; i++) {
                this.reserversDom[i].style.display = 'inline';
            }
        } else {
            this.statutDom.classList.remove("--statut-open");
            this.statutDom.classList.add("--statut-close");
            for (var i = 0; i < this.reserversDom.length; i++) {
                this.reserversDom[i].style.display = 'none';
            }
        }

        for (var i = 0 ; i < this.reserversDom.length ; i++) {
            if(station.nombre_velos_disponible <= 0) {
                this.reserversDom[i].style.display = 'none';
            }
        }

        this.identifiantDom.innerHTML = station.identifiant;

        this.statutDom.innerHTML = 'Station '+ station.statut;

        this.nomDom.innerHTML = station.nom.split(' - ')[1];

        this.adresseDom.innerHTML = station.adresse;

        this.emplacementsDisposDom.innerHTML = station.nombre_emplacements_libre;

        this.velosDisposDom.innerHTML = station.nombre_velos_disponible;

        if(station.nombre_velos_reserves > 0) {
            document.getElementsByClassName('js-velo-reserve')[0].innerHTML = '/ 1 vélo réservé';
        } else {
            document.getElementsByClassName('js-velo-reserve')[0].innerHTML = '';
        }
    };

    /**
     * [description]
     * @return {[type]} [description]
     */
    APPLICATION.MODULES.Carte.prototype.get_markerIdAffiche = function() {
        return this.markerIdAffiche;
    };

    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* Velib ---------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */

    /**
     * [description]
     * @return {[type]} [description]
     */
    APPLICATION.MODULES.Velib = function() {
        this.stations = [];

        this.__construct();
    };

    /**
     * [description]
     * @param  {[type]} idHtmlMap [description]
     * @return {[type]}           [description]
     */
    APPLICATION.MODULES.Velib.prototype.__construct = function(idHtmlMap) {
        var _this = this;

        var liste = null;

        APPLICATION.application.outils.recuperer_donnees_get(
            "https://opendata.paris.fr/api/records/1.0/search/?dataset=stations-velib-disponibilites-en-temps-reel&lang=fr&rows=-1&start=0",
            function(reponse) {
                _this.generer_stations(reponse);
            }
        );
    };

    /**
     * [description]
     * @param  {[type]} reponse [description]
     * @return {[type]}         [description]
     */
    APPLICATION.MODULES.Velib.prototype.generer_stations = function(reponse) {
        var liste = JSON.parse(reponse);

        if(!liste.records) {
            console.log('Aucunes donnees depuis l\'API');
            return false;
        }

        for (var i = 0 ; i < liste.records.length ; i++) {
            this.stations[i] = new APPLICATION.MODELES.Station(liste.records[i], i);
            // voir pour créer automatiquement les markers en même temps
            // this.markers.push(this.generer_marker(this.listeStations[i]))
        }

        APPLICATION.application.__constructSuite();

        return this;
    };

    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* Reservation ---------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */

    /**
     * [description]
     * @return {[type]} [description]
     */
    APPLICATION.MODULES.Reservation = function() {
        this.reservation = null;

        this.canvas = null;

        this.canvasQuitterDom = null;

        this.canvasEffacerDom = null;

        this.resaDecompteDom = null;

        this.resaDateStartDom = null;

        this.resaDateStopDom = null;

        this.resaSignatureDom = null;

        this.resaAnnulerDom = null;

        this.HeadresaNumberDom = null;

        this.HeadInfoResaDom = null;

        this.HeadVoirResaDom = null;

        this.dessin = null;

        this.tempsRestantMinutes = null;

        this.tempsRestantSecondes = null;

        this.informationsResa = null;

        this.informationsTexte = null;

        this.intervalId = null;

        this.__construct();
    };

    /**
     * [description]
     * @return {[type]} [description]
     */
    APPLICATION.MODULES.Reservation.prototype.__construct = function() {
        var _this = this;

        this.canvas = new APPLICATION.MODULES.Caneva('#canvas');

        var reserversDom = APPLICATION.application.carte.reserversDom;

        for (var i = 0; i < reserversDom.length; i++) {
            reserversDom[i].addEventListener('click', function(e){return _this.signer(e);});
        }

        this.canvasQuitterDom = document.getElementsByClassName('js-caneva-quitter')[0];

        this.canvasQuitterDom.addEventListener('click', function(e){
            e.preventDefault();

            return _this.canvas.quitter();
        });

        this.canvasEffacerDom = document.getElementsByClassName('js-caneva-effacer')[0];

        this.canvasEffacerDom.addEventListener('click', function(e){
            e.preventDefault();

            return _this.canvas.effacer();
        });

        this.canvasEnregistrerDom = document.getElementsByClassName('js-caneva-enregistrer')[0];

        this.canvasEnregistrerDom.addEventListener('click', function(e){
            e.preventDefault();

            return _this.verifier(e);
        });

        this.resaDecompteDom = document.getElementsByClassName('js-decompte')[0];

        this.resaDateStartDom = document.getElementsByClassName('js-reservation-heure-start')[0];

        this.resaDateStopDom = document.getElementsByClassName('js-reservation-heure-stop')[0];

        this.resaSignatureDom = document.getElementsByClassName('js-reservation-signature')[0];

        this.resaAnnulerDom = document.getElementsByClassName('js-reservation-annuler')[0];

        this.HeadVoirResaDom = document.getElementById('js-head-voir-resa');

        this.HeadVoirResaDom.addEventListener('click', function(e){
            return _this.voir_resa_head(e);
        }, false);

        this.HeadInfoResaDom = document.getElementsByClassName('js-head--info-resa')[0];

        this.HeadresaNumberDom = document.getElementsByClassName('js-head-resa-number')[0];

        this.resaAnnulerDom.addEventListener('click', function(e){
            return _this.arreter_reservation(e);
        }, false);

        this.informationsResa = document.getElementsByClassName('js-informations-resa')[0];

        this.informationsTexte = document.getElementsByClassName('js-information-texte')[0];

        this.gerer_informations('initialisation');

        /* si resa */

        if(localStorage.getItem('resaId') && localStorage.getItem('resaId') !== null) {
            this.reservation = new APPLICATION.MODELES.Reservation(null, null, true);

            if(this.reservation.verifier_validite()) {
                var date = new Date();

                var changeMinutes = false;

                if(date.getMinutes() === this.reservation.minuteFinChiffre) {
                    this.tempsRestantMinutes = 0;
                } else {
                    if(this.reservation.minuteFinChiffre - date.getMinutes() < 0) {
                        this.tempsRestantMinutes = (60-date.getMinutes()) + this.reservation.minuteFinChiffre;
                    } else {
                        this.tempsRestantMinutes = this.reservation.minuteFinChiffre - date.getMinutes();
                    }

                }

                if(date.getSeconds() === this.reservation.secondeFinChiffre) {
                    this.tempsRestantSecondes = 0;
                } else {
                    if(this.reservation.secondeFinChiffre - date.getSeconds() < 0) {
                        this.tempsRestantSecondes = (60-date.getSeconds()) + this.reservation.secondeFinChiffre;
                    } else {
                        this.tempsRestantSecondes = this.reservation.secondeFinChiffre - date.getSeconds();
                    }

                }

                this.HeadresaNumberDom.innerHTML = 1;

                this.HeadInfoResaDom.innerHTML = 'Vous avez une reservation sur ' + APPLICATION.application.stations.stations[this.reservation.resaId].nom + ' qui se terminera ' + this.reservation.tempsFinLettre;

                this.resaDateStartDom.innerHTML = this.reservation.tempsDebutLettre;

                this.resaDateStopDom.innerHTML = this.reservation.tempsFinLettre;

                this.resaDecompteDom.innerHTML = this.tempsRestantMinutes+':' + this.tempsRestantSecondes;

                this.resaSignatureDom.src = this.reservation.signature;

                this.gerer_informations('resa');

                this.intervalId = setInterval(function() {
                    return _this.decompter();
                }, 10000);
            } else {
                console.log('stop resa delai ecoule');
                this.arreter_reservation(null);
            }
        }

        return this;
    };



    /**
     * [description]
     * @param  {[type]} e [description]
     * @return {[type]}   [description]
     */
    APPLICATION.MODULES.Reservation.prototype.signer = function(e) {
        e.preventDefault();

        this.canvas.afficher();
    };

    /**
     * [description]
     * @param  {[type]} e [description]
     * @return {[type]}   [description]
     */
    APPLICATION.MODULES.Reservation.prototype.verifier = function(e) {
        e.preventDefault();

        e.stopPropagation();

        if(this.reservation) {
            this.arreter_reservation();
        }

        var _this = this;

        this.dessin = this.canvas.exporter();

        this.canvas.effacer();

        this.canvas.quitter();

        var idStation = APPLICATION.application.carte.get_markerIdAffiche();

        idStation = Number(idStation);

        if(!idStation) {
            console.log('l\'identifiant de la station est invalide');

            return false;
        }

        if(!APPLICATION.application.stations.stations[idStation]) {
            console.log('Il n\'existe pas de stations avec cet identfiant');

            return false;
        }

        this.reservation = new APPLICATION.MODELES.Reservation(
            APPLICATION.application.stations.stations[idStation],
            this.dessin
        );

        APPLICATION.application.stations.stations[idStation].nombre_velos_reserves = 1;

        APPLICATION.application.carte.fermer_marker(APPLICATION.application.carte.markers[idStation]);

        this.generer();

        APPLICATION.application.modale.afficher('La réservation débute. Vous avez 20 minutes pour retirer votre vélo.');

        return this;
    };

    /**
     * [description]
     * @return {[type]} [description]
     */
    APPLICATION.MODULES.Reservation.prototype.generer = function() {
        var _this = this;

        this.tempsRestantMinutes = 20;

        this.tempsRestantSecondes = 0;

        this.HeadresaNumberDom.innerHTML = 1;

        this.HeadInfoResaDom.innerHTML = 'Vous avez une reservation sur ' + APPLICATION.application.stations.stations[this.reservation.resaId].nom + ' qui se terminera ' + this.reservation.tempsFinLettre;

        this.resaDateStartDom.innerHTML = this.reservation.tempsDebutLettre;

        this.resaDateStopDom.innerHTML = this.reservation.tempsFinLettre;

        this.resaDecompteDom.innerHTML = '20:00';

        this.resaSignatureDom.src = this.dessin;

        localStorage.clear();

        for (var i in this.reservation) {
            if (this.reservation.hasOwnProperty(i)) {
                localStorage.setItem(i, this.reservation[i]);
            }
        }

        this.gerer_informations('resa');

        this.intervalId = setInterval(function() {
            return _this.decompter();
        }, 10000);

        return this;
    };

    /**
     * [description]
     * @return {[type]} [description]
     */
    APPLICATION.MODULES.Reservation.prototype.decompter = function() {
        var minuteChange = false;
        if(this.tempsRestantSecondes-10 < 0) {
            minuteChange = true;
            this.tempsRestantSecondes = 60 - 10;
        }else {
            this.tempsRestantSecondes -= 10;
        }

        if(minuteChange) {
            if(this.tempsRestantMinutes > 0) {
                this.tempsRestantMinutes -= 1;
            }
        }

        if(this.tempsRestantMinutes === 0) {
            if(this.tempsRestantSecondes === 0) {
                this.arreter_reservation();
            }
        }

        var affichage = this.tempsRestantMinutes + ':' + (this.tempsRestantSecondes < 10?'0' + (this.tempsRestantSecondes):this.tempsRestantSecondes);

        this.resaDecompteDom.innerHTML = affichage;

        console.log(affichage);

        return affichage;
    };

    /**
     * [description]
     * @param  {[type]} rendu [description]
     * @return {[type]}       [description]
     */
    APPLICATION.MODULES.Reservation.prototype.gerer_informations = function(rendu) {
        var texteDom = null;

        switch (rendu) {
            case 'initialisation':
                texteDom = document.createTextNode('Aucune réservation');

                this.informationsTexte.appendChild(texteDom);

                this.informationsResa.style.display = 'none';
            break;

            case 'resa':
                this.informationsTexte.firstChild.nodeValue = 'Une réservation en cours';

                this.informationsResa.style.display = 'block';
            break;

            case 'resaStop':
                this.informationsTexte.firstChild.nodeValue = 'Reservation annulée';

                this.informationsResa.style.display = 'none';
            break;
        }

        return this;
    };

    /**
     * [description]
     * @param  {[type]} [e=null] [description]
     * @return {[type]}          [description]
     */
    APPLICATION.MODULES.Reservation.prototype.arreter_reservation = function(e) {
        if(e === undefined) {
            e = null;
        }

        if(e !== null) {
            e.preventDefault();
        }

        var idStation = APPLICATION.application.carte.get_markerIdAffiche();

        if(e === undefined) {
            APPLICATION.application.carte.fermer_marker(null, APPLICATION.application.carte.markers[idStation]);
        }

        APPLICATION.application.stations.stations[localStorage.getItem('resaId')].nombre_velos_reserves = 0;

        this.reservation = null;

        localStorage.clear();

        clearInterval(this.intervalId);

        this.gerer_informations('resaStop');

        this.HeadresaNumberDom.innerHTML = 0;

        this.HeadInfoResaDom.innerHTML = 'aucune reservation';

        this.tempsRestantMinutes = 20;

        this.tempsRestantSecondes = 0;

        this.resaDateStartDom.innerHTML = '';

        this.resaDateStopDom.innerHTML = '';

        this.resaSignatureDom.src = '';

        localStorage.removeItem("reservation");

        for (var i in this.reservation) {
            if (this.reservation.hasOwnProperty(i)) {
                // console.log(i);
                // console.log(this.reservation[i]);
                localStorage.removeItem(i);
            }
        }

        localStorage.clear();

        APPLICATION.application.modale.afficher('La réservation est annulée.');

        return this;
    };

    APPLICATION.MODULES.Reservation.prototype.voir_resa_head = function() {
        if(this.HeadInfoResaDom.classList.contains("cache")) {
            this.HeadInfoResaDom.classList.remove("cache");
        } else {
            this.HeadInfoResaDom.classList.add("cache");
        }
    };

    APPLICATION.MODULES.Reservation.prototype.quitter = function(e) {
        console.log('quitter');
        for (var i in this.reservation) {
            if (this.reservation.hasOwnProperty(i)) {
                // console.log(i);
                // console.log(this.reservation[i]);
                localStorage.removeItem(i);
            }
        }
    };

    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* Caneva --------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */

    /**
     * [description]
     * @param  {[type]} idDom [description]
     * @return {[type]}       [description]
     */
    APPLICATION.MODULES.Caneva = function(idDom) {
        this.canvasConteneur = null;
        this.canevas = null;
        this.canevasDom = null;

        this.stopDessin = true;
        this.coords = [];
        this.coord = [];
        this.posX = null;
        this.posY = null;
        this.x = null;
        this.xPrec = null;
        this.y = null;
        this.yPrec = null;

        return this.__construct(idDom);
    };

    /**
     * [description]
     * @param  {[type]} idDom [description]
     * @return {[type]}       [description]
     */
    APPLICATION.MODULES.Caneva.prototype.__construct = function(idDom) {
        var _this = this;
        this.canvasConteneur = document.getElementsByClassName('js-caneva-conteneur')[0];

        this.canevasDom  = document.querySelector('#canvas');

        this.canevas = this.canevasDom.getContext('2d');

        this.canevasDom.addEventListener('mousedown', function(e){ return _this.activer_dessin(e);});

        this.canevasDom.addEventListener('mouseup', function(e){ return _this.stopper_dessin(e);});

        this.canevasDom.addEventListener('mouseleave', function(e){ return _this.stopper_dessin(e);});

        this.canevasDom.addEventListener('mouseleave', function(e){ return _this.stopper_dessin(e);});

        return this;
    };

    /**
     * [description]
     * @return {[type]} [description]
     */
    APPLICATION.MODULES.Caneva.prototype.stopper_dessin = function() {
        var _this = this;

        this.stopDessin = true;

        this.canevasDom.removeEventListener("mousemove", function(){});

        return this;
    };

    /**
     * [description]
     * @param  {[type]} e [description]
     * @return {[type]}   [description]
     */
    APPLICATION.MODULES.Caneva.prototype.activer_dessin = function(e) {
        var _this = this;

        var x, xPrec, y, yPrec = null;

        this.stopDessin = false;

        var idNav = APPLICATION.application.outils.detecter_navigateur();

        if(idNav === 1) {
            this.x = e.clientX-this.canevasDom.offsetLeft;

            this.y = e.clientY-this.canevasDom.offsetTop;
        } else if(idNav === 4) {
            this.x = e.clientX-this.canevasDom.offsetLeft;

            _this.y = e.clientY-_this.canevasDom.offsetTop;
        } else {
            this.x = e.layerX-this.canevasDom.offsetLeft;

            this.y = e.layerY-this.canevasDom.offsetTop;
        }

        this.xPrec = this.x;

        this.yPrec = this.y;

        this.canevasDom.addEventListener('mousemove', function(e){
            if(!_this.stopDessin) {
                var idNav = APPLICATION.application.outils.detecter_navigateur();

                if(idNav === 1) {
                    _this.x = e.clientX-_this.canevasDom.offsetLeft;

                    _this.y = e.clientY-_this.canevasDom.offsetTop;
                } else if(idNav === 4) {
                    _this.x = e.clientX-_this.canevasDom.offsetLeft;

                    _this.y = e.clientY-_this.canevasDom.offsetTop;
                }  else {
                    _this.x = e.layerX-_this.canevasDom.offsetLeft;

                    _this.y = e.layerY-_this.canevasDom.offsetTop;
                }

                _this.canevas.beginPath();

                _this.canevas.moveTo(_this.xPrec, _this.yPrec);

                _this.canevas.lineTo(_this.x, _this.y);

                _this.canevas.strokeStyle = "rgb(0, 0, 0)";

                _this.canevas.stroke();

                _this.canevas.closePath();

                _this.xPrec = _this.x;

                _this.yPrec = _this.y;
            } else {
                _this.stopper_dessin();
            }
        });

        return this;
    };

    /**
     * [description]
     * @return {[type]} [description]
     */
    APPLICATION.MODULES.Caneva.prototype.exporter = function() {
        var dataURL = canvas.toDataURL();

        return dataURL;
    };

    /**
     * [description]
     * @return {[type]} [description]
     */
    APPLICATION.MODULES.Caneva.prototype.effacer = function() {
        return this.canevas.clearRect(0, 0, 300, 200);
    };

    /**
     * [description]
     * @return {[type]} [description]
     */
    APPLICATION.MODULES.Caneva.prototype.afficher = function() {
        this.canevas.clearRect(0, 0, 300, 200);

        return this.canvasConteneur.style.visibility = "visible";
    };

    /**
     * [description]
     * @return {[type]} [description]
     */
    APPLICATION.MODULES.Caneva.prototype.quitter = function() {
        this.canevas.clearRect(0, 0, 300, 200);

        return this.canvasConteneur.style.visibility = "hidden";
    };

    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* Modale --------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */

    APPLICATION.MODULES.Modale = function() {
        this.active = false;

        this.modaleContenuDom = null;

        this.modaleDom = null;

        this.__construct();
    };

    APPLICATION.MODULES.Modale.prototype.__construct = function() {
        if(!document.getElementsByClassName('modale')[0]) {
            console.log('Impossible d\initialiser la modale');
            return false;
        }

        this.modaleDom = document.getElementsByClassName('modale')[0];

        if(!document.getElementsByClassName('modale-contenu')[0]) {
            console.log('Impossible d\initialiser la modale-contenu');
            return false;
        }

        this.modaleContenuDom = document.getElementsByClassName('modale-contenu')[0];

        this.active = true;

        return this;
    };

    APPLICATION.MODULES.Modale.prototype.afficher = function(texte) {
        if(!this.active) {
            console.log('modale est inactive. Pas d\'affichage');
            return false;
        }

        var _this = this;

        this.modaleContenuDom.innerHTML = texte;

        this.modaleDom.style.visibility = "visible";

        setTimeout(
            function() {
                return _this.cacher();
            },
            4000
        );
    };

    APPLICATION.MODULES.Modale.prototype.cacher = function() {
        if(!this.active) {
            console.log('modale est inactive. Pas d\'affichage');
            return false;
        }

        this.modaleDom.style.visibility = "hidden";

        this.modaleContenuDom.innerHTML = '';
    };


    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* Erreur --------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */

    APPLICATION.MODULES.Erreur = function() {

    };
})();
(function() {
    "use strict";

    APPLICATION.MODULES.Diaporama = function(diaporamaClassName, diaporamaDom) {

        /** vitesse de défilement de la slide */
        this.sliderContainerDom = '';

        /** vitesse de défilement de la slide */
        this.sliderContainerDomLargeur = '';

        /** vitesse de défilement de la slide */
        this.sliderContainerDomHauteur = '';

        // bouton pour passer à la slide precedente
        this.precedenteDom = null;

        // bouton pour passer à la slide suivante
        this.suivanteDom = null;

        /** vitesse de défilement de la slide */
        this.slides = [];

        /** identifie le numéro de la slide qui sera active */
        this.slideActiveNumero = 0;

        /** node DOM de la slide active */
        this.slideActiveDom = null;

        /** node DOM de la slide suivant la slide active */
        this.slideSousActiveDom = null;

        /** node DOM de la slide suivant la slide active */
        this.slideSuivanteDom = null;

        /** node DOM de la slide precedant la slide active */
        this.slidePrecedanteDom = null;

        /** demarrage automatique du diaporama */
        this.slideTime = 4000;

        /** vitesse de défilement de la slide */
        this.vitesse = 50;

        /** demarrage automatique du diaporama */
        this.autoplay = true;

        /**  */
        this.intervalId = null;

        /**  */
        this.animationId = null;

        this.initialiser(diaporamaClassName, diaporamaDom);

        return this;
    };

    APPLICATION.MODULES.Diaporama.prototype.avancer_slide = function() {
        this.slideSousActiveDom = this.slideSuivanteDom;

        this.slideSousActiveDom.classList.add('js-slide--suivante');

        var xBloc = parseFloat(getComputedStyle(this.slideActiveDom).left);

        if (xBloc <= this.sliderContainerDomLargeur) {
            this.slideActiveDom.style.left = (xBloc + this.vitesse) + "px";

            var _this = this;

            this.animationId = requestAnimationFrame(function() {
                return _this.avancer_slide();
            });

        } else {

            if(((this.slideActiveNumero+1)) >= this.slides.length) {
                this.slideActiveNumero = 0;
            } else {
                this.slideActiveNumero++;
            }

            this.slideActiveDom.classList.remove('js-slide--active');

            this.slideActiveDom.style.left = "0px";

            this.slideActiveDom = this.slideSousActiveDom;

            this.sliderContainerDom.style.height = this.slideActiveDom.offsetHeight + 'px';

            this.slideActiveDom.classList.add('js-slide--active');

            this.slideActiveDom.classList.remove('js-slide--suivante');

            if(((this.slideActiveNumero+1)) >= this.slides.length) {
                this.slideSuivanteDom = this.slides[0];
            } else {
                this.slideSuivanteDom = this.slides[this.slideActiveNumero+1];
            }

            if((this.slideActiveNumero-1) < 0) {
                this.slidePrecedanteDom = this.slides[this.slides.length-1];
            } else {
                this.slidePrecedanteDom = this.slides[this.slideActiveNumero-1];
            }

            cancelAnimationFrame(this.animationId);
        }

        return this;
    };

    APPLICATION.MODULES.Diaporama.prototype.reculer_slide = function() {
        this.slideSousActiveDom = this.slidePrecedanteDom;

        this.slideSousActiveDom.classList.add('js-slide--suivante');

        var xBloc = parseFloat(getComputedStyle(this.slideActiveDom).left);

        if (xBloc <= this.sliderContainerDomLargeur) {
            this.slideActiveDom.style.left = (xBloc + this.vitesse) + "px";

            var _this = this;

            this.animationId = requestAnimationFrame(function() {
                return _this.reculer_slide();
            });
        } else {
            if(((this.slideActiveNumero-1)) < 0) {
                this.slideActiveNumero = this.slides.length-1;
            } else {
                this.slideActiveNumero--;
            }

            this.slideActiveDom.classList.remove('js-slide--active');

            this.slideActiveDom.style.left = "0px";

            this.slideActiveDom = this.slideSousActiveDom;

            this.sliderContainerDom.style.height = this.slideActiveDom.offsetHeight + 'px';

            this.slideActiveDom.classList.add('js-slide--active');

            this.slideActiveDom.classList.remove('js-slide--suivante');

            if(((this.slideActiveNumero+1)) >= this.slides.length) {
                this.slideSuivanteDom = this.slides[0];
            } else {
                this.slideSuivanteDom = this.slides[this.slideActiveNumero+1];
            }

            if((this.slideActiveNumero-1) < 0) {
                this.slidePrecedanteDom = this.slides[this.slides.length-1];
            } else {
                this.slidePrecedanteDom = this.slides[this.slideActiveNumero-1];
            }

            cancelAnimationFrame(this.animationId);
        }

        return this;
    };

    APPLICATION.MODULES.Diaporama.prototype.deplacerBloc = function () {
        var xBloc = parseFloat(getComputedStyle(this.slideActiveDom).left);

        if (xBloc <= this.sliderContainerDomLargeur) {
            this.slideActiveDom.style.left = (xBloc + this.vitesse) + "px";

            var _this = this;

            this.animationId = requestAnimationFrame(function() {
                return _this.deplacerBloc();
            });
        } else {
            this.slideActiveDom.classList.remove('js-slide--active');

            this.slideActiveDom.style.left = "0px";

            this.slideActiveDom = this.slideSuivanteDom;

            this.sliderContainerDom.style.height = this.slideActiveDom.offsetHeight + 'px';

            this.slideActiveDom.classList.add('js-slide--active');

            this.slideActiveDom.classList.remove('js-slide--suivante');

            this.slideActiveNumero++;

            if((this.slideActiveNumero) >= this.slides.length) {
                this.slideActiveNumero = 0;

                this.slideSuivanteDom = this.slides[this.slideActiveNumero];
            } else {
                this.slideSuivanteDom = this.slides[this.slideActiveNumero];
            }

            this.slideSuivanteDom.classList.add('js-slide--suivante');

            cancelAnimationFrame(this.animationId);
        }
        return this;
    };

    APPLICATION.MODULES.Diaporama.prototype.gerer_evenement_clavier = function(e) {
        var _this = this;
        // console.log(e.keyCode);
        // console.log("e clavier " + e.type + " , sur la touche " + e.keyCode);
        switch (e.keyCode) {
            case 39:
                this.avancer_slide();
            break;

            case 37:
                this.reculer_slide();
            break;

            default:

            break;
        }
    };

    APPLICATION.MODULES.Diaporama.prototype.verifier_autoplay = function(diaporamaDom) {
        if(!diaporamaDom.getAttribute("js-diaporama-autoplay")) {
            return true;
        }

        var autoplay = diaporamaDom.getAttribute("js-diaporama-autoplay");

        if(autoplay === 'false' || autoplay === false) {
            return false;
        }

        return true;
    };

    APPLICATION.MODULES.Diaporama.prototype.verifier_slideTime = function(diaporamaDom) {
        if(!diaporamaDom.getAttribute("js-diaporama-time")) {
            return 4000;
        }

        var time = Number((diaporamaDom.getAttribute("js-diaporama-time")));

        if(time < 900 || time > 10000) {
            return 4000;
        }

        return time;
    };

    APPLICATION.MODULES.Diaporama.prototype.ajuster_hauteur = function() {
        this.sliderContainerDom.style.height = this.slideActiveDom.offsetHeight + 'px';

        return this;
    };

    APPLICATION.MODULES.Diaporama.prototype.initialiser = function(diaporamaClassName, diaporamaDom) {
        var _this = this;

        this.sliderContainerDom = diaporamaDom;

        this.sliderContainerDomLargeur = parseFloat(getComputedStyle(this.sliderContainerDom).width);

        this.sliderContainerDomHauteur = parseFloat(getComputedStyle(this.sliderContainerDom).height);

        this.slides = document.querySelectorAll('.' + diaporamaClassName + ' ' + '.js-slide');

        if(this.slides.length < 1) {
            console.log('Le nombre de slides : ' + this.slides.length + '. Arret du diaporama');
            return false;
        }

        if(this.slides.length < 2) {
            console.log('Le nombre de slides : ' + this.slides.length + '. Arret du diaporama');
            return false;
        }

        this.slideTime = this.verifier_slideTime(diaporamaDom);

        this.autoplay = this.verifier_autoplay(diaporamaDom);

        this.slideActiveDom = this.slides[this.slideActiveNumero];

        if(((this.slideActiveNumero+1)) >= this.slides.length) {
            this.slideSuivanteDom = this.slides[0];
        } else {
            this.slideSuivanteDom = this.slides[this.slideActiveNumero+1];
        }

        if((this.slideActiveNumero-1) < 0) {
            this.slidePrecedanteDom = this.slides[this.slides.length-1];
        } else {
            this.slidePrecedanteDom = this.slides[this.slideActiveNumero-1];
        }

        this.precedenteDom = document.getElementsByClassName('js-diaporama-prec')[0];

        this.precedenteDom.addEventListener("click", function(e) {
            e.preventDefault();
            _this.reculer_slide(e);
        });

        this.suivanteDom = document.getElementsByClassName('js-diaporama-suiv')[0];

        this.suivanteDom.addEventListener("click", function(e) {
            e.preventDefault();
            _this.avancer_slide(e);
        });

        this.slideActiveDom.classList.add('js-slide--active');

        this.sliderContainerDom.style.height = this.slideActiveDom.offsetHeight + 'px';

        body.addEventListener("keydown", function(e) {
            e.preventDefault();
            _this.gerer_evenement_clavier(e);
        });

        if(this.autoplay) {
            this.intervalId = setInterval(function() {
                return _this.deplacerBloc();
            }, this.slideTime);
        }

        window.addEventListener("resize", function() {
            return _this.ajuster_hauteur();
        });

        return this;
    };

    APPLICATION.MODULES.DiaporamaGestionnaire = function() {
        this.diaporamaClassName = "js-diaporama";

        this.diaporamasDom = document.getElementsByClassName(this.diaporamaClassName);

        this.diaporamas = [];

        for (var i = 0; i < this.diaporamasDom.length; i++) {
            this.diaporamas.push(new APPLICATION.MODULES.Diaporama(this.diaporamaClassName, this.diaporamasDom[i]));
        }
    };
    // var diaporamas = new DiaporamaGestionnaire();
})();
