var APPLICATION = APPLICATION || {};

(function() {
    "use strict";
    /* espaces de noms */
    APPLICATION.MODELES = APPLICATION.MODELES || {};

    APPLICATION.MODULES = APPLICATION.MODULES || {};

    APPLICATION.DONNEES = APPLICATION.DONNEES || {};

    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* Application ---------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */

    /**
     * Objet Application
     * @return objet
     */
    APPLICATION.Application = function() {
        var _this = this;

        this.outils = null;

        this.carte = null;

        this.stations = null;

        this.reservation = null;

        this.modale = null;

        window.addEventListener(
            "load",
            function() {
                _this.__construct();
            },
            false
        );

        return this;
    };

    /**
     * Constructeur de l'objet Application
     * @return objet
     */
    APPLICATION.Application.prototype.__construct = function() {
        var _this = this;

        this.outils = new APPLICATION.MODULES.Outils();

        this.diaporama = new APPLICATION.MODULES.DiaporamaGestionnaire();

        this.carte = new APPLICATION.MODULES.Carte('map');

        this.stations = new APPLICATION.MODULES.Velib();

        this.modale = new APPLICATION.MODULES.Modale();

        return this;
    };

    /**
     * Construire la suite de l'application
     * @return objet
     */
    APPLICATION.Application.prototype.__constructSuite = function() {
        this.carte.generer_markers(this.stations.stations);

        this.reservation = new APPLICATION.MODULES.Reservation();

        return this;
    };

    // Lancement de l'application
    APPLICATION.application = new APPLICATION.Application();
})();
