var APPLICATION = APPLICATION || {};

(function() {
    "use strict";

    APPLICATION.DONNEES.applicationConfiguration = {
        nom: 'CycloP',
        ecrans: {
            jetable: 300,
            smarphone: 500,
            tablette: 800,
            minipc: 1024,
            pc: 1300
        },
        map: {
            options: {
                zoom: 12,
                center: {
                    lat: 48.8613894683,
                    lng: 2.34005523282
                }
            },
            clusters: {
                options: {
                    textColor: 'white',
                    url: 'public/medias/images/application/map_icone.png',
                    height: 50,
                    width: 50,
                    textSize: '14'
                }
            }
        },
        reservation: {
            duree: 20
        }
    };

    APPLICATION.DONNEES.applicationTextes = {
        erreur: {
            erreur: 'L\'application rencontre un problème. Recharger la page.',
            reservationAnnulee: 'La réservation est annulée.',
            reservationDemaree: 'La réservation démarre. Vous avez $1 minutes, soit jusqu\'a $2 pour récupérer votre vélo.'
        },
        reservation: {
            infoResaActive: '1 réservation active',
            infoResaInactive: 'Pas de réservation',
            nouvelleModale: 'La réservation démarre. Vous avez $1 minutes, soit jusqu\'a $2 pour récupérer votre vélo.',
            annuleeModale: 'La réservation est annulé.',
            remplaceModale: 'La réservation démarre. Elle remplace la précédente. Vous avez $1 minutes, soit jusqu\'a $2 le $3 pour récupérer votre vélo.',
            pasDeResaActive: 'Aucune réservation active.'
        }
    };

    APPLICATION.DONNEES.erreurs = {
        utilisateur: {
            quitter: false,
            afficher: true
        },
        application: {
            quitter: false,
            afficher: true
        }
    };

    APPLICATION.DONNEES.mapOptions = {
        zoom: 12,
        center: {
            lat: 48.8613894683,
            lng: 2.34005523282
        }
    };

    APPLICATION.DONNEES.clusterStyles = [
        {
            textColor: 'white',
            url: 'public/medias/images/application/map_icone.png',
            height: 50,
            width: 50,
            textSize: '14'
        }
    ];
})();
