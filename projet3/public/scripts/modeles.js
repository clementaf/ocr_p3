(function() {
    "use strict";

    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* Station -------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */

    /**
     * objet Station
     * @param  object objectJSON objet tiré de l'API ville de Paris
     * @param  int i          identifiant du tableau stations (qui regroupe toutes les stations)
     * @return objet            objet de type station
     */
    APPLICATION.MODELES.Station = function(objectJSON, i) {
        this.identifiant = i;

        this.numero = objectJSON.fields.number;

        this.statut = (objectJSON.fields.status === "OPEN"?"ouverte":"fermée");

        this.nom = objectJSON.fields.name;

        this.adresse = objectJSON.fields.address;

        this.ville = objectJSON.fields.contract_name;

        this.borne_payement = objectJSON.fields.banking;

        this.bonus = objectJSON.fields.bonus;

        this.nombre_emplacements = objectJSON.fields.bike_stands;

        this.nombre_emplacements_libre = objectJSON.fields.available_bike_stands;

        this.nombre_velos_disponible = objectJSON.fields.available_bikes;

        this.nombre_velos_reserves = 0;

        this.latitude = objectJSON.fields.position[0];

        this.longitude = objectJSON.fields.position[1];

        return this;
    };

    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* Marker --------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */

    /**
     * objet Marker
     * @param  objet stationObjet objet de type Station
     * @return objet              objet de type Marker
     */
    APPLICATION.MODELES.Marker = function(stationObjet) {
        return this.__construct(stationObjet);
    };

    /**
     * constructeur pour l'objet Marker
     * @param  objet stationObjet objet de type Station
     * @return objet              objet Marker
     */
    APPLICATION.MODELES.Marker.prototype.__construct = function(stationObjet) {
        var _this = this;

        var marker = {
            position: {lat: Number(stationObjet.latitude), lng:Number(stationObjet.longitude)},
            clickable: true,
            title: stationObjet.nom,
            icon: 'public/medias/images/application/map_marker.png',
            idStation: stationObjet.identifiant
        };
        return marker;
    };

    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* Reservation ---------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */

    /**
     * objet de type Reservation
     * @param  objet station objet de type Station
     * @param  objet caneva  objet canvas
     * @return objet         objet de type Reservation
     */
    APPLICATION.MODELES.Reservation = function(station, caneva, localStorage) {
        this.resaId = null;

        this.anneeFinChiffre = null;

        this.moisFinChiffre = null;

        this.jourFinChiffre = null;

        this.heureFinChiffre = null;

        this.minuteFinChiffre = null;

        this.secondeFinChiffre = null;

        this.tempsDebutChiffre = null;

        this.tempsDebutLettre = null;

        this.tempsFinChiffre = null;

        this.tempsFinLettre = null;

        this.signature = null;

        return this.__construct(station, caneva, localStorage);
    };

    /**
     * constructeur de l'objet Reservation
     * @param  {[type]} station [description]
     * @param  {[type]} caneva  [description]
     * @return objet         objet Reservation
     */
    APPLICATION.MODELES.Reservation.prototype.__construct = function(station, caneva, localStorage) {
        if(localStorage && station === null && caneva === null) {
            for (var i in this) {
                if (this.hasOwnProperty(i)) {
                    // console.log(i);
                    // console.log(this.reservation[i]);
                    this[i] = window.localStorage.getItem(i);
                }
            }
        } else {
            this.definir_dates();

            this.resaId = station.identifiant;

            this.signature = caneva;
        }


        return this;
    };

    /**
     * definir les dates et horaire de la Reservation
     * @return objet         this
     */
    APPLICATION.MODELES.Reservation.prototype.definir_dates = function() {
        var date = new Date();

        var heureDebut, heureFin, minuteDebut, minuteFin, jourDebut, jourFin, jourSemaineDebut, jourSemaineFin, nombreJourMax, moisDebut, moisFin, anneeDebut, anneeFin, reste;
        var heureChange, jourChange, moisChange, anneeChange = false;
        var nombreJourParMois = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        var jourSemaine = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche'];
        var moisAnnee = ['janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];

        // minute
        reste = 0;
        minuteDebut = date.getMinutes();

        this.heureDebutChiffre = null;

        this.minuteDebutChiffre = null;

        if(minuteDebut + 20 > 59) {
            heureChange = true;

            minuteFin = 20 - (59-minuteDebut+1);
        } else {
            minuteFin = minuteDebut + 20;
        }

        this.minuteFinChiffre = minuteFin;

        this.secondeFinChiffre = date.getSeconds();

        // heure
        heureDebut = date.getHours();
        reste = 0;

        if(heureChange) {
            if((heureDebut + 1) > 23) {
                jourChange = true;
                heureFin = 0;
            } else {
                heureFin = heureDebut + 1;
            }
        }else {
            heureFin = heureDebut;
        }

        this.heureFinChiffre = heureFin;

        // jourSemaine, jour, mois, annee
        jourSemaineDebut = date.getDay();
        jourDebut = date.getDate();
        moisDebut = date.getMonth();
        anneeDebut = date.getFullYear();
        nombreJourMax = nombreJourParMois[moisDebut];
        reste = 0;

        if(jourChange) {
            if((jourDebut + 1) > nombreJourMax) {
                moisChange = true;
                jourFin = 1;
            } else {
                jourFin = jourDebut + 1;
            }

            if((jourSemaineDebut + 1) > 6) {
                jourSemaineFin = 0;
            } else {
                jourSemaineFin = jourSemaineDebut+1;
            }
        }else {
            jourSemaineFin = jourSemaineDebut;
            jourFin = jourDebut;
        }

        if(moisChange) {
            if((moisDebut + 1) > 11) {
                anneeChange = true;
                moisFin = 0;
            } else {
                moisFin = moisDebut + 1;
            }
        }else {
            moisFin = moisDebut;
        }

        if(anneeChange) {
            anneeFin = anneeDebut+1;
        }else {
            anneeFin = anneeDebut;
        }

        this.anneeFinChiffre = anneeFin;

        this.moisFinChiffre = moisFin;

        this.jourFinChiffre = jourFin;

        this.tempsDebutChiffre = (heureDebut<10?'0' + (heureDebut):heureDebut) + ':' + (minuteDebut<10?'0' + (minuteDebut):minuteDebut) + '  -  ' + (jourDebut<10?'0' + (jourDebut):jourDebut) + '/'+ (moisDebut+1<10?'0' + (moisDebut+1):moisDebut+1) + '/' + anneeDebut;

        this.tempsDebutLettre = 'le ' + jourSemaine[jourSemaineDebut] + ' ' + (jourDebut<10?'0' + (jourDebut):jourDebut) + ' ' + moisAnnee[moisDebut] + ' ' + anneeDebut + ' à ' + (heureDebut<10?'0' + (heureDebut):heureDebut) + 'H' + (minuteDebut<10?'0' + (minuteDebut):minuteDebut);

        this.tempsFinChiffre = (heureFin<10?'0' + (heureFin):heureFin) + ':' + (minuteFin<10?'0' + (minuteFin):minuteFin) + '  -  ' + (jourFin<10?'0' + (jourFin):jourFin) + '/'+ (moisFin+1<10?'0' + (moisFin+1):moisFin+1) + '/' + anneeFin;

        this.tempsFinLettre = 'le ' + jourSemaine[jourSemaineFin] + ' ' + (jourFin<10?'0' + (jourFin):jourFin) + ' ' + moisAnnee[moisFin] + ' ' + anneeFin + ' à ' + (heureFin<10?'0' + (heureFin):heureFin) + 'H' + (minuteFin<10?'0' + (minuteFin):minuteFin);

        return this;
    };

    /**
     * definir les dates et horaire de la Reservation
     * @return objet         this
     */
    APPLICATION.MODELES.Reservation.prototype.verifier_validite = function() {

        var date = new Date();

        if(date.getFullYear() >= this.anneeFinChiffre && date.getMonth() >= this.moisFinChiffre && date.getDay() >= this.jourFinChiffre && date.getHours() >= this.heureFinChiffre && date.getMinutes() >= this.minuteFinChiffre && date.getSeconds() >= this.secondeFinChiffre) {
            console.log('pb date');
            return false;
        }

        return true;
    };
})();
