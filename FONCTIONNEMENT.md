# Fonctionnement de l'application CycloP

## L'application

L'application utilise HTML, CSS et Javascript (pas JQUERY).

### Javascript

L'utilisation de Javascript plutôt que JQUERY sont :
- comprendre enfin son fonctionnement natif sans surcouche
- eviter de charger une bibliothèque
- coder pour réutiliser (L'implémentation de Javascript auprès de chaque navigateur est satisfaisante)

Utilisation de l'instruction "use strict"; pour garantir un code respectueux des standards.

### Organisation du code Javascript

>*Problématique : plusieurs contraintes se sont imposées au fur et à mesure*
- repartir le code contenu dans application dans des éléments plus petit
- repartir inteligement le code
- ne pas charger trop de fichiers
- pouvoir réutiliser le code
- séparer (sur le format MVC par exemple)

#### Utilisation de *NAMESPACES*

Au nombre de quatre, ils évitent la surcharge sur l'objet Window en réduisant le nombres d'élements directement accessible (ce qui évite des conflits avec d'autres biblitothèques) et améliore la séparation du code en interne de l'application.

__APPLICATION__
> Le NAMESPACE APPLICATION est attaché à l'espace global window. C'est le seul accessible depuis cet espace. Il est le grand chef d'orchestre.

__APPLICATION.MODELES__
> Le NAMESPACE MODELE est attaché au NAMESPACE APPLICATION. Sont créés dans ce NAMESPACE les modèles de données

__APPLICATION.MODULES__
> Le NAMESPACE MODULES est attaché au NAMESPACE APPLICATION. Sont créés dans ce NAMESPACE des éléments (qui se veulent réutilisables) comprenant de la logique et des vues

__APPLICATION.DONNEES__
> Le NAMESPACE DONNEES est attaché au NAMESPACE APPLICATION. Contient des tableaux/objets utilisables par des MODULES ou des MODELES

#### Les Classes (objet par référence)

Afin de garder une large compatibilité, l'utilisation du mot clé "class" n'est pas utilisé pour la création des classes (utilisable depuis ES6). De même, il n'y à pas de valeur par défaut dans les variables de fonctions.

L'ensemble des ces classes sont regroupés dans 4 fichiers Javascript.

__La classe du NAMESPACE APPLICATION__

*Application*  initialise tous les objets utiles à l'application.

__La classe du NAMESPACE MODELES__

*Station* initialise une Station pour chaque objet tirés de l'API Paris.

*Marker* initialise tous les Markers de la google map. Utilise des objets Station.

*Reservation* initialise tous les informations d'une réservation.

__La classe du NAMESPACE MODULES__

*Outils* regroupe des fonctions qui n'ont pas de classe propres.

*Carte* gère la carte, les markers, l'information au clic d'un marker et les clusters (groupe de markers).

*Velib* gère les stations (et les Stations).

*Reservation* gère la réservation (et la Réservation).

*Caneva* gère le canvas.

*Modale* gère l'affichage de 'fenetre' à l'écran.

*DiaporamaGestionnaire* gère le diaporama (slider). [se parametre avec des éléments data]

#### Schéma
![Organisation](Ressources/organisation_schema.jpg)

## Déroulé

Le diaporama est initialisé au départ.

Puis la map, la récupération des données et la création des stations.

Ensuite création des markers.

## Améliorations

- Créer en Javascript Modale, Canvas et fenêtre de réservation.

- ajouter des options a modale (durée d'affichage (oui/non), fermer la modale avec une croix)

- Utiliser Modale pour afficher le canvas. Modale accepterai un tableau noeud DOM (premier niveau qui serait enfant de la fenetre modale) contenant toute les infos à afficher.

- Améliorer systeme d'information pour la résa.

- stocker plus d'éléments sur données afin de facilité les changements (textes, nombres...)

- ajouter une infos/alerte sur le temps restant à 5 minutes
