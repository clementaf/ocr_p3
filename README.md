UNE NOUVELLE VERSION EXISTE SUR LA BRANCHE 'developpement'

Elle inclue

- un modele DateTime (qui permet un abstraction des datetime au niveau de Reservation
- un modele Storage qui gère le localStorage
- une séparation des fichiersjs par type. L'ensemble des styles et scripts sont gérés par Gulp pour réduire le poid et le nombre de fichier (maintenant, en dehord des bibliothèques externes, il y a un fichier js et un fichier css)

# Application CycloP

L'application web Cyclop permet de simuler la réservation d'un vélo (Vélib).

Les données sont issu de l'API de la Ville de Paris.

## Fonctionnalités

### Diaporama

Le diaporama permet de connaitre le fonctionnement de l'application. Il réagit au touches "flèche gauche / flèche droite" pour respectivement reculer/avancer d'une slide.

### carte et reservation

Les cercles représentent des groupes de markers. Un marker indique l'emplacement d'un parc de vélo. Vous pouvez cliquez sur un marker pour connaitre les détails :
    - état (ouvert ou fermé)
    - son nom
    - nombre de vélo disponibles
    - nombre d'emplacements

Si un vélo est disponible, vous pouez le réserver contre signture. A la validation, vous aurez 20 minutes pour retirer votre vélo.

La réservatino est annulé si vous dépassez ces vingts minutes, si vous annuler la réservation ou si vous créez une nouvelle réservation.

### Responsive

L'ensemble de la page est adaptable. Même lors d'un redimensionnement du navigateur.

### Compatibilité des navigateurs

L'application fonctionne sur les navigateurs suivant :
- Firefox >=53
- Chrome (last)
- Opéra  (last)
- IE11  (last)
- Edge  (last)

La compatibilité est assuré par un code JS le plus simple possible (instructions et règles de syntaxe).

[Comprendre l'application](FONCTIONNEMENT.md)
